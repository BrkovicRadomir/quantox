<?php
session_start();
define('APP_ROOT', dirname(__DIR__));
define('VIEWS', dirname(__DIR__)."/app/Views/");
define('MODELS', dirname(__DIR__)."/app/Models/");
define('REQUESTS', dirname(__DIR__)."/app/Requests/");




// routing

include (APP_ROOT."/app/System/Bootstrap.php");
use App\System\Services\Configure;
use App\System\Services\Routes as Routes;
//include (APP_ROOT."/app/System/Services/Routes.php");

$config =  new Configure();

define("BASE_URL", $config->getConfig('baseUrl'));

$routing = new Routes();


?>
