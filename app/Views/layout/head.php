<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php  echo isset($title) ? $title : "This is test"; ?></title>
    <link rel="stylesheet" href="<?= BASE_URL."/css/bootstrap.min.css" ?>">
</head>
<body>
<section>
    <?php if(isset($message)){ ?>
    <div class="alert alert-<?= isset($msgClass) ? $msgClass : "warning"?>">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?= $message ?>
    </div>

    <?php } ?>

    <?php if(\App\System\Auth::check()){ ?>
        <a href="<?= BASE_URL."/logout" ?>" class="btn-sm btn-danger" style="margin: 10px; float: right;">Logout</a>
    <?php } ?>


