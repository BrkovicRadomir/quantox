<?php include_once(VIEWS."/layout/head.php"); ?>


<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
        <h1><?= $title ?></h1>

            <form method="get" class="form-inline col-lg-12" style="margin-bottom: 50px;" action="<?= BASE_URL."/users" ?>">
                <div class="form-group">
                    <input class="form-control" name="q" value="<?= $request->get('q') ?>">
                </div>

                    <button class="btn-sm btn-success" type="submit">Search</button>

            </form>

        <?php if($users->count() > 0) { ?>
                <table class="table">
                    <?php foreach($users->result() as $user) { ?>
                            <tr>
                                <td><?= $user->name ?></td>
                                <td><?= $user->email ?></td>

                            </tr>
                    <?php }?>
                </table>

        <?php } else {?>

            <p>Sorry, but we didn't found any user.</p>

        <?php } ?>

    </div>
</div>



<?php include_once(VIEWS."/layout/footer.php"); ?>
