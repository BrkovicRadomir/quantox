<?php include_once(VIEWS."/layout/head.php"); ?>


<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
        <h1><?= $title ?></h1>
        <form action="<?= BASE_URL."/authentication"  ?>" method="POST">
            <div class="form-group <?= isset($errors['email']) ?  "has-error" : null ?> ">
                <label for="email">
                    Email
                </label>
                <input name="email" id="email" class="form-control" value="<?= isset($inputs['email'])? $inputs['email'] : null  ?>" >
                <?= isset($errors['email'])? '<p class="help-block">'.$errors['email']."</p>" : null  ?>
            </div>

            <div class="form-group <?= isset($errors['password']) ?  "has-error" : null ?> ">
                <label for="password">
                    Password
                </label>
                <input name="password" id="password" type="password" class="form-control" value="<?= isset($inputs['password'])? $inputs['password'] : null  ?>" >
                <?= isset($errors['password'])? '<p class="help-block">'.$errors['password']."</p>" : null  ?>
            </div>



            <div class="form-group">

                <button type="submit" class="btn-success btn-lg">Submit</button>
                <a href="<?= BASE_URL."/registration" ?>" style="float: right;">Registration</a>
            </div>

            <form>
    </div>
</div>



<?php include_once(VIEWS."/layout/footer.php"); ?>
