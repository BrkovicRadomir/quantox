<?php

namespace App\Requests;


use App\System\Services\Validation;

class LoginRequest extends Validation
{
    /**
     * Set rules for my validation
     * @return array
     */
    public function rules(){
        return [
            'password' => 'required',
            'email' => "required|email"
        ];
    }


    /**
     * Get custom messages
     * @return array
     */
    protected function messages(){
        return [
            'password' => [
                'required' => "The password is required field."
            ],
            'email' => [
                'required' => "The email is required field.",
                'email' => "The email must be a valid email address."
            ]
        ];


    }

}