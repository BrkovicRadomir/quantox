<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 1.7.17.
 * Time: 11.11
 */

namespace App\Requests;


use App\System\Services\Validation;

class RegistrationRequest extends Validation
{

    /**
     * Set rules for my validation
     * @return array
     */
    public function rules(){
        return [
            'name' => "required",
            'confirm_password' => "password_confirm|required",
            'password' => 'required',
            'email' => "email|required"
        ];
    }


    /**
     * Get custom messages
     * @return array
     */
    protected function messages(){
        return [
            'name' => [
                'required' => "The name is required field."
            ],

            'password' => [
                'required' => "The password is required field."
            ],
            'email' => [
                'required' => "The email is required field.",
                'email' => "The email must be a valid email address."
            ],
            'confirm_password' => [
                'required' => "The confirm password is required field.",
                'password_confirm' => "The password and confirm password are not the same fields."
            ]
        ];


    }
}