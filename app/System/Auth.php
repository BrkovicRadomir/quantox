<?php

namespace App\System;



use App\System\Services\Authentication;

class Auth
{


    private static $authentication = null;

    // Check is user logged
    public static function check(){
        $authentication = self::init();
        return $authentication->checkIsLogged();
    }


    // Try to login user
    public static function login($inputs){
        $authentication = self::init();
        return $authentication->attempt($inputs);
    }


    // get logged user
    public static function user(){
        $authentication = self::init();
        return $authentication->getLoggedUser();
    }


    // logout user
    public static function logout(){
        $authentication = self::init();
        $authentication->logoutUser();
    }

    // login user by id
    public static function loginById($id){
        $authentication = self::init();
        return $authentication->loginUserById($id);
    }

    /**
     * @return Authentication|null
     */
    private static function init(){

        if(self::$authentication == null){
            self::$authentication = new Authentication();
            self::$authentication->setModelName('User');
        }


        return self::$authentication;
    }
}