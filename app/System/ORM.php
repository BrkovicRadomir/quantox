<?php

namespace App\System;


use App\System\Services\DB;

class  ORM extends DB
{

    /**
     * @param array $fields
     * @return mixed
     */
    public function create(array $fields){
        $this->insert($fields);
        $item = $this->orderBy('id', "DESC")->get();
        return $item->result()[0];

    }


    /**
     * @param $id
     * @return mixed
     */
    public function find($id){
        $output = [];
        $item = $this->getById($id);

        $output = $item->fetchAll(\PDO::FETCH_CLASS);
        if(isset($output[0])){
            $this->attributes = (array) $output[0];

            return $this;
        } else
            return null;

    }




}
