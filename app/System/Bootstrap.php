<?php


$bootstrapFiles = [
    [
        'file' => "/app/System/Services/Routes.php",
        'name' => "Routes"
    ],
    [
        'file' => "/app/System/Services/Request.php",
        'name' => "Request"
    ],
    [
        'file' =>    "/app/System/Services/Validation.php",
        'name' => "Validation"
    ],
    [
        'file' =>    "/app/System/Services/Response.php",
        'name' => "Response"
    ],
    [
        'file' => "/app/System/Services/DB.php",
        'name' => "DB"
    ],
    [
        'file' =>  "/app/System/Services/View.php",
        'name' => 'DB'
    ],

    [
        'file' =>  "/app/System/ORM.php",
        'name' => "ORM"
    ],
    [
        'file' => "/app/System/Controller.php",
        'name' => "Controller"
    ],
    [
        'file' => "/app/System/Services/Collection.php",
        'name' => "Collection"
    ],
    [
        'file' => "/app/System/Services/Authentication.php",
        'name' => "Authentication"
    ],
    [
        'file' => "/app/System/Auth.php",
        'name' => "Auth"
    ]





];



foreach($bootstrapFiles as $file){
    if(file_exists(APP_ROOT.$file['file'])){
        __autoload($file);
      //  include_once(APP_ROOT.$file);
    } else {
        echo "File {$file['file']} doesn't exist. <br>";
    }
}

function __autoload($file){
    include_once(APP_ROOT.$file['file']);
}