<?php

namespace App\System;

use App\System\Services\Response;
use App\System\Services\View;

class Controller
{

    /**
     * @param $name
     * @param array|null $data
     */
    protected function view($name, array $data = null){
        $view = new View();

        $view->setView($name, $data);
    }

    /**
     * @param $model
     * @return mixed
     */
    protected function setModel($model){
        $object = null;

        if(file_exists(MODELS.$model.".php")){
            include(MODELS.$model.".php");

            $model = 'App\Models\\'.$model;
            return new $model;
        } else {
            // TODO::throw exception

            echo "Model ".MODELS.$model.".php doesn't exist.";
        }
    }


    /**
     * @param $request
     * @return mixed
     */
    protected function setRequest($request){
        $object = null;

        if(file_exists(REQUESTS.$request.".php")){
            include(REQUESTS.$request.".php");

            $request = 'App\Requests\\'.$request;
            return new $request;
        } else {
            // TODO::throw exception

            echo "Model ".REQUESTS.$request.".php doesn't exist.";
        }
    }


    protected function response(){
        $response = new Response();
        return $response;
    }


    protected function errors(){
        return $GLOBALS['errors'];
    }

    protected function inputs(){
        return $GLOBALS['inputs'];
    }

    protected function message(){
        return $GLOBALS['message'];
    }

    protected function msgClass(){
        return $GLOBALS['msg-class'];
    }

}