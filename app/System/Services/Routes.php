<?php


namespace App\System\Services;


class Routes
{

    public function __construct(){

        $url = $this->getCurrentUri();

        $method = isset($_POST['_method']) ? $_POST['_method'] : $_SERVER['REQUEST_METHOD'];

        //$request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

        $this->routes($url, $method);

    }

    /**
     * @param $url
     * @param $method
     */
    protected function routes($url, $method){
        if($url == '/' and $method == "GET"){
            include (APP_ROOT."/app/Controllers/HomeController.php");
            $home = new \App\Controllers\HomeController();
            return $home->index();
        } else {

            $this->sessionsResponse();
            $routes = json_decode(file_get_contents(APP_ROOT.'/routes.json'), true);
            $url = explode('/', $url);
            foreach($routes['routes'] as $route){

                if (strpos($route['url'], $url[1]) !== FALSE and $route['type'] == $method){
                    if(count($url) == 2){
                        $controller = $this->setController($route);
                        return $controller->{$route['method']}();
                    } else {
                        $routeUrl = explode('/', $route['url']);
                        $success = 1;
                        $arguments = "";
                        for($i = 1; $i < count($url); $i++){
                            if($routeUrl[$i] == $url[$i] or (($routeUrl[$i] == "any" and is_string($url[$i])) or ($routeUrl[$i] == "numb" and is_numeric($url[$i])))){
                              $success++;
                              if($routeUrl[$i] == "any" or $routeUrl[$i] == "numb"){
                                $arguments .= $url[$i];
                              }
                            } else {
                                break;
                            }
                        }

                        if($success == count($url)){
                            $controller = $this->setController($route);
                            return $controller->{$route['method']}($arguments);
                        }
                    }



                }
            }

            echo "Page not found <br>";
        }
    }

    protected function sessionsResponse(){
        $GLOBALS = $_SESSION;
        unset($_SESSION['inputs']);
        unset($_SESSION['errors']);
        unset($_SESSION['message']);
        unset($_SESSION['msg-class']);

    }


    /**
     * @return string
     */
    protected function getCurrentUri()
    {
        $basepath =  implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
        $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
        if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
        $uri = '/' . trim($uri, '/');
        return $uri;
    }

    /**
     * @param $route
     * @return string
     */
    protected function setController($route){

        include (APP_ROOT."/app/Controllers/{$route['controller']}.php");

        $controller = '\App\Controllers\\'.$route['controller'];
        $controller = new $controller;

        return $controller;
    }




}