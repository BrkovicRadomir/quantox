<?php

namespace App\System\Services;


class Configure
{
  public function getConfig($index = null){
    $conf = json_decode(file_get_contents(APP_ROOT.'/conf.json'), true);

    if($index and isset($conf[$index]))
      return $conf[$index];

    else
      return $conf;

  }
}
