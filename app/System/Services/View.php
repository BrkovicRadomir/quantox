<?php

namespace App\System\Services;


class View
{


    /**
     * @param $name
     * @param array|null $data
     */
    public function setView($name, array $data = null){
        $name = explode('.', $name);
        $name = implode('/', $name);

        if(file_exists(VIEWS.$name.".php")){

            if($data != null)
                extract($data);
                include(VIEWS.$name.".php");

        } else {
            //TODO::throw exception
            echo "View ".$name." doesn't exist";
        }
    }

}