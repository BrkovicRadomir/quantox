<?php

namespace App\System\Services;


class Validate
{

    protected $passed = false;


    /**
     * @param array $inputs
     * @param array $rules
     * @return $this
     */
    public function  make(array $inputs, array $rules){

        foreach($rules as $key => $row){
           $this->passed = $this->$row($key, $inputs);
        }

        return $this;


    }


    /**
     * @return bool
     */
    public function fails(){
       return $this->passed ? false : true;
    }


    /**
     * @param $key
     * @param $inputs
     * @return bool
     */
    private function required($key, $inputs){
       if(isset($inputs[$key]) and !empty($inputs[$key]))
            return true;
        else
            return false;
    }

    /**
     * @param $key
     * @param $inputs
     * @return bool
     */
    private function email($key, $inputs){
        if(isset($inputs[$key])){
            if(filter_var($inputs[$key], FILTER_VALIDATE_EMAIL))
                return true;
            else
                return false;
        } else
            return true;
    }

    /**
     * @param $key
     * @param $inputs
     * @return bool
     */
    private function password_confirm($key, $inputs){
        if((isset($inputs['password']) and !empty($inputs['password'])) and ($inputs[$key]) == $inputs['password'])
            return true;
         else
            return false;
    }
}