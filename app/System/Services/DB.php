<?php

namespace App\System\Services;

include('Configure.php');

class DB {

    // reading configuration file
    protected $conf = [];
    // orm table
    protected $table;
    // PDO Connection
    protected $con = null;

    // Conditional

    protected $conditional = "";

    protected $fields = "*";

    protected $orderBy;

    protected $limit;

    protected $attributes = [];


    function __construct($conf = null)
    {
        if (!$conf) {
            $conf = new Configure();
            $this->conf = $conf->getConfig('database');

        } else
            $this->conf = $conf;


        $dsn = "mysql:dbname={$this->conf['database']};host={$this->conf['hostname']}";

        try {
            $this->con = new \PDO($dsn, $this->conf['username'], $this->conf['password']);
        } catch (\PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }

    }


    /**
     * @param $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }



    /**
     * @param array|string $fields
     * @return $this
     */
    public function select($fields){
        if(is_array($fields))
            $fields = implode(',', $fields);

        if($this->fields)
            $this->fields .= ", ".$fields;
        else
            $this->fields = $fields;

        return $this;
    }


    /**
     * @param string $field
     * @param mixed $value
     * @param null $sign
     * @return $this
     */
    public function where($field, $value, $sign = null)
    {
        if (!$sign)
            $sign = "=";


        if(is_string($value))
            $value = '\''.$value.'\'';

        if ($this->conditional) {
            $this->conditional .= " AND {$field} {$sign} {$value}";
        } else
            $this->conditional = "{$field} {$sign} {$value}";

        return $this;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param null $sign
     * @return $this
     */
    public function orWhere($field, $value, $sign = null)
    {
        if (!$sign)
            $sign = "=";

        if(is_string($value))
            $value = '\''.$value.'\'';

        if ($this->conditional) {
            $this->conditional .= " OR {$field} {$sign} {$value}";
        } else
            $this->conditional = "{$field} {$sign} {$value}";

        return $this;
    }

    /**
     * @param string $field
     * @param array $value
     * @return $this
     */
    public function whereIn($field, array $value)
    {
        if ($this->conditional) {
            $this->conditional .= " AND {$field} IN (".implode(', ', $value).")";
        } else
            $this->conditional = " {$field} IN (".implode(', ', $value).")";

        return $this;
    }

    /**
     * @param string $field
     * @param array $value
     * @return $this
     */
    public function orWhereIn($field, array $value)
    {
        if ($this->conditional) {
            $this->conditional .= " OR {$field} IN (".implode(', ', $value).")";
        } else
            $this->conditional = " {$field} IN (".implode(', ', $value).")";

        return $this;
    }

    /**
     * @param $column
     * @param string $type
     * @return $this
     */
    public function orderBy($column, $type = "ASC"){
        $this->orderBy = " ORDER BY {$column} {$type}";
        return $this;

    }

    /**
     * @return mixed
     */
    public function get(){

        $cond = "";

        if($this->conditional)
            $cond = "WHERE {$this->conditional}";

        if($this->orderBy)
            $cond .= $this->orderBy;

        if($this->limit)
            $cond .= $this->limit;

        $query = "SELECT {$this->fields} FROM {$this->table} ".$cond;

        $items = $this->con->query($query);
        if($items){
            $collection = new Collection();
            $output = $items->fetchAll(\PDO::FETCH_CLASS);

            foreach($output as $result){
                $this->attributes = (array) $result;

                $collection->addItem(clone $this);

            }

            return $collection;

        } else
            return null;



    }

    /**
     * @return mixed
     */
    public function all(){
        $output = [];

        $query = "SELECT {$this->fields} FROM {$this->table} ";

        $items = $this->con->query($query);

        $items = $this->con->query($query);
        if($items){
            $collection = new Collection();
            $output = $items->fetchAll(\PDO::FETCH_CLASS);

            foreach($output as $result){
                $this->attributes = (array) $result;

                $collection->addItem(clone $this);

            }

            return $collection;

        } else
            return null;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id){
        $query = "SELECT {$this->fields} FROM {$this->table} WHERE id = {$id}";

        return $this->con->query($query);
    }


    /**
     * @param array $fields
     */
    public function insert(array $fields){
        $value = [];
        $field = [];
        $i = 0;
        foreach ($fields as $key => $row){

            $value[$i] = ":".$key;
            $field[$i] = $key;

            $i++;
        }

        $query = $this->con->prepare("INSERT INTO {$this->table} (".implode(',', $field).") VALUES (".implode(',', $value).")");
        $query->execute($fields);

    }

    /**
     * @param array $values
     */
    public function update(array $values){
        $field = [];

        foreach ($values as $key => $row){
            $field[] =$key." = :".$key;

        }
        $cond = "";

        if($this->conditional)
            $cond = "WHERE {$this->conditional}";
        elseif(isset($this->attributes['id']))
            $cond = "WHERE id = {$this->attributes['id']}";


        $query = $this->con->prepare("UPDATE {$this->table}   SET ".implode(',', $field)." {$cond}");
        $query->execute($values);
    }


    /**
     * delete row
     */
    public function delete(){
        $cond = "";

        if($this->conditional)
            $cond = "WHERE {$this->conditional}";

        $query = $this->con->prepare("DELETE FROM  {$this->table}  {$cond}");

        $query->execute();
    }


    /**
     * @param $key
     * @return null
     */
    public function __get($key){
        if(array_key_exists($key, $this->attributes)){
            return $this->attributes[$key];
        } else
            return null;
    }


}
