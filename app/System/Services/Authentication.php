<?php

namespace App\System\Services;


class Authentication
{


    private $sessionName = "user";

    private $model = "User";

    protected $loggedUser = null;


    /**
     * @param $name
     * @return $this
     */
    public function setSessionName($name){
        $this->sessionName = $name;
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public  function setModelName($name){
        $this->model = $name;
        return $this;
    }

    /**
     * @return null
     */
    public function getLoggedUser(){
        return $this->loggedUser;
    }


    /**
     * @return bool
     */
    public function checkIsLogged(){
        if(isset($_SESSION[$this->sessionName])){
            if($this->checkHasUser()){
                return true;
            } else
                return false;
        } else {
            return false;
        }
    }


    /**
     * @param $inputs
     * @return bool
     */
    public function attempt($inputs){

        $model = $this->getModel();
        if($model != null){
            $users = $model;
            foreach($inputs as $key => $value){
                $users = $users->where($key, $value);
            }

            $users = $users->get();
            if($users->count() > 0){
                foreach($users->result() as $user){
                    $this->loginUser($user);
                    break;
                }
                return true;
            } else {
                return false;
            }
        } else
            return false;


    }

    /**
     * Logout user
     */
    public function logoutUser(){
        unset($_SESSION[$this->sessionName]);
        $this->loggedUser = null;
    }


    /**
     * @param $id
     * @return bool
     */
    public function loginUserById($id){
        $model = $this->getModel();

        if($model != null){
            $user = $model->find($id);

            if($user){
                $this->loginUser($user);
                return true;
            } else {
                return false;
            }
        } else
            return false;
    }




    /**
     * @return bool
     */
    private function checkHasUser(){
        $model = $this->getModel();
        // check is model exist
        if($model != null){
            $loggedUser = $model->find($_SESSION[$this->sessionName]);

            // check is logged user exist in database
            if($loggedUser){
                $this->loggedUser = $loggedUser;
                return true;
            } else {

                $this->loggedUser = null;
                return false;
            }

        } else
            return false;

    }

    /**
     * @return bool
     */
    final private function getModel(){
        $modelPath = MODELS."/{$this->model}.php";
        // check is file model exits
        if(file_exists($modelPath)){
            include_once($modelPath);
            $model = 'App\Models\\'.$this->model;
            return new $model;

        } else {
            return false;
        }
    }

    /**
     * @param $loggedUser
     */
    private function loginUser($loggedUser){
        $this->loggedUser = $loggedUser;
        $_SESSION[$this->sessionName] = $loggedUser->id;
    }



}