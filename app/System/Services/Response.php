<?php

namespace App\System\Services;


class Response
{

    /**
     * @param array $data
     * @param int $httpCode
     */
    public function json( array $data, $httpCode = 200){
        header('Content-Type: application/json');
        http_response_code($httpCode);
        echo json_encode($data);
    }

    /**
     * @return $this
     */
    public function back(){

        header('Location: ' . $_SERVER['HTTP_REFERER']);

        return $this;
    }


    /**
     * @param array $data
     * @return $this
     */
    public function with(array $data){
       foreach($data as $key => $row){
          $_SESSION[$key] = $row;
       }

        return $this;
    }

    /**
     * @param $url
     * @return $this
     */
    public function redirect($url){
        header('Location: ' . $url);

        return $this;
    }


}