<?php


namespace App\System\Services;


class Collection
{


    protected $items = [];



    /**
     * @param $value
     */
    public function addItem($value){
        $this->items[] = $value;
    }

    /**
     * @return array
     */
    public function result(){
        return $this->items;
    }



    /**
     * @return int
     */
    public function count(){
        return count($this->items);
    }



}