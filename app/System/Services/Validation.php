<?php

namespace App\System\Services;


abstract class Validation extends Request
{

    abstract protected function rules();
    abstract protected function messages();

    protected $messages = [];
    protected $errors = [];
    protected $passed = false;



    /**
     * @return bool
     */
    public function isPassed(){
        $this->validate();


        return $this->passed;
    }

    /**
     * @return $this
     */
   final private function validate(){
        $rules = $this->rules();
        $this->messages = $this->messages();
       $inputs = $this->all();
        include_once ('Validate.php');

        $passed = 0;
        $possible = 0;

       foreach($rules as  $key => $row){
            // get rules for field
           if(!is_array($row))
               $ruleArray = explode('|', $row);
           else
               $ruleArray = $row;

            foreach($ruleArray as $rule){
                $validate = new Validate();
                // get rules with or operator
                $orRules = explode('/', $rule);
                if(count($orRules) > 1) {
                    $passed += $this->orValidation($inputs, $validate, $orRules, $key, $passed);

                } else {
                    $validation = $validate->make($inputs, [
                        $key => $rule
                    ]);

                    if(!$validation->fails()){
                        $passed++;
                    } else {
                        $this->errors[$key] = $this->errorMessages($rule, $key);
                    }
               }
                $possible ++;
            }


        }



        if($possible == $passed)
            $this->passed = true;

        return $this;
    }


    /**
     * @param $inputs
     * @param $validate
     * @param $orRules
     * @param $key
     * @param $passed
     * @return mixed
     */
    final private function orValidation($inputs, $validate, $orRules, $key, $passed){

            $i = 1;
            foreach($orRules as $orRule) {
                $orValidate = $validate->make($inputs, [
                    $key => $orRule
                ]);

                if (!$orValidate->fails()) {
                    $passed++;
                    break;
                } else {
                    if ($i == count($orRules))
                        $this->errors[$key] = $this->errorMessages($orRule, $key);
                }
                $i++;
            }


        return $passed;
    }


    /**
     * @param $rule
     * @param $field
     * @return mixed
     */
    final private function errorMessages($rule, $field){
        if(isset($this->messages[$field])){
            $messages = $this->messages[$field];
            if(isset($messages[$rule]))
                return $messages[$rule];
            else
                return $this->defaultMessages()[$rule];

        }
        else {
            $messages = $this->defaultMessages();

            return $messages[$rule];
        }

    }


    /**
     * @param null $key
     * @return array|null
     */
    final public function errors($key = null){
        if($key == null)
            return $this->errors;
        else {
            if(isset($this->errors[$key]))
                return $this->errors[$key];
            else
                return null;
        }
    }


    /**
     * @return array
     */
    final private function defaultMessages(){
        return [
          'required'   =>   "The field is required.",
          'url'        =>   'The url format is not valid.',
          'email'      =>   'The email must be a valid email address.'
        ];
    }

}