<?php


namespace App\Controllers;


use App\System\Auth;
use App\System\Controller;
use App\System\Services\Request;

class UserController extends Controller
{

    protected $users;

    public function __construct(){
        $this->users = $this->setModel("User");
    }


    /**
     * Show list of users
     */
    public function index(){

        if(!Auth::check())
            $this->response()->with([
                'message' => "Please login"
            ])->redirect(BASE_URL);


        $message = $this->message();
        $msgClass = $this->msgClass();

        $request = new Request();

        $users = $this->users;

        if($request->get('q')){
            $users = $users->where('name', "%{$request->get('q')}%", "LIKE")
                            ->orWhere('email', "%{$request->get('q')}%", "LIKE");
        }

        $users = $users->orderBy('name', "ASC")->get();
        $this->view('users.index', ['title' => "Users", 'message' => $message, 'msgClass' => $msgClass, 'users' => $users, 'request' => $request ]);
    }

}