<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 1.7.17.
 * Time: 09.31
 */

namespace App\Controllers;


use App\System\Auth;
use App\System\Controller;

class LoginController extends Controller
{

    /**
     * Show form for login
     */
    public function login(){
        $errors = $this->errors();
        $inputs = $this->inputs();
        $message = $this->message();

        $this->view('login.index', [
            'errors' => $errors,
            'inputs' => $inputs,
            'title' => "Login",
            'message' => $message
        ]);

    }

    /**
     * Get authentication
     */
    public function authentication(){

        $request = $this->setRequest("LoginRequest");

        if($request->isPassed()){

            if(Auth::login([
                'email' => $request->get('email'),
                'password' => md5($request->get('password'))
            ])){
                 $this->response()->with([
                     'message' => "Welcome ".Auth::user()->name,
                     'msg-class' => "success"
                 ])->redirect(BASE_URL."/users");
            } else {
                $this->response()->with([
                    'message' => "User with this credentials not exist in database",
                    'msg-class' => 'warning'
                ])->back();
            }

        } else {
            $this->response()->with(['inputs' => $request->all(),
                'errors' => $request->errors()])->back();
        }
    }

    public function logout(){
        Auth::logout();

        $this->response()->redirect(BASE_URL);
    }

}