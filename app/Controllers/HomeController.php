<?php

namespace App\Controllers;

use App\System\Auth;
use App\System\Controller;
use App\System\Services\Request;

class HomeController extends Controller
{


    public function index(){

       if(Auth::check()) {
           $this->response()->redirect(BASE_URL.'/users');
       } else {
           $this->response()->redirect(BASE_URL.'/login');
       }

    }



}