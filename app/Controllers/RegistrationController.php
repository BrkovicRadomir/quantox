<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 1.7.17.
 * Time: 11.10
 */

namespace App\Controllers;


use App\System\Auth;
use App\System\Controller;

class RegistrationController extends Controller
{


    /**
     * Show registration form
     */
    public function index(){
        $errors = $this->errors();
        $inputs = $this->inputs();
        $message = $this->message();

        $this->view('registration.index', [
            'errors' => $errors,
            'inputs' => $inputs,
            'title' => "Registration",
            'message' => $message
        ]);

    }


    /**
     * Registration user
     */
    public function store(){
        // Validate form request
        $request = $this->setRequest('RegistrationRequest');

        if($request->isPassed()){
           $userModel = $this->setModel('User');

            // create new user
            $user = $userModel->create([
               'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => md5($request->get('password'))
            ]);

            // login new user
            Auth::loginById($user->id);


            $this->response()->with([
                'message' => "Welcome ".$user->name,
                'msg-class' => "success"
            ])->redirect(BASE_URL."/users");


        } else {
            $this->response()->with(['inputs' => $request->all(),
                'errors' => $request->errors()])->back();
        }
    }
}