<?php

namespace App\Models;


use App\System\ORM;

class User extends ORM
{

    public function __construct(){
        parent::__construct();
        $this->setTable("users");
    }
}